import pandas as pd


class TaxonomyImporter:
    """
    Class used to import taxonomy from Catalogue of life.
    Provided files come from Catalogue of Life Checklist 2021-09-21 and contain all taxa belonging to Chordata,
    excluding ranks below subspecies; they are in coldp native format.
    The file is ordered in a particular way: under class record there are all the taxa belonging to the class,
    below that there is a section starting with another class and listing all its members etc.
    Taxon.csv file contains list of taxa with information about their parent taxon, taxon rank and latin name.
    VernacularName.tsv contains species' names in several languages (only english is used).
    """

    CHORDATA_CLASSES = [
        'Actinopterygii',
        'Amphibia',
        'Aves',
        'Cephalaspidomorphi',
        'Leptocardii',
        'Elasmobranchii',
        'Holocephali',
        'Mammalia',
        'Myxini',
        'Reptilia',
        'Sarcopterygii',
        'Appendicularia',
        'Ascidiacea',
        'Thaliacea'
    ]

    def __init__(
            self,
            taxon_file="Taxon.tsv",
            name_file="VernacularName.tsv",
    ):
        # tables preprocessing
        taxa = pd.read_csv(taxon_file, sep='\t').fillna("")
        taxa = taxa.loc[taxa["dwc:taxonomicStatus"] == "accepted"]
        taxa = taxa.loc[taxa["dwc:taxonRank"].isin(["class", "order", "family", "genus", "species", "subspecies"])]
        # only leave columns with data used to import taxa
        taxa = taxa[["dwc:taxonID", "dwc:parentNameUsageID", "dwc:taxonRank", "dwc:scientificName", "dwc:genericName",
                     "dwc:infragenericEpithet", "dwc:specificEpithet", "dwc:infraspecificEpithet"]]

        # generate latin names by concatenating values from 4 columns, because scientificName column
        # also contains name and year the species was described
        taxa["latin_name"] = taxa.apply(lambda row: self.concatenate_taxon_name(row), axis=1)
        taxa = taxa[["dwc:taxonID", "dwc:parentNameUsageID", "dwc:taxonRank", "latin_name"]]
        taxa = taxa.rename(columns={"dwc:taxonID": "taxon_id", "dwc:parentNameUsageID": "parent_taxon_id",
                                    "dwc:taxonRank": "taxon_rank"})

        names = pd.read_csv(name_file, sep='\t')
        names = names.loc[names["dcterms:language"] == "eng"]
        names = names.drop_duplicates("dwc:taxonID")
        names = names[["dwc:taxonID", "dwc:vernacularName"]]
        names = names.rename(columns={"dwc:taxonID": "taxon_id", "dwc:vernacularName": "english_name"})

        # self.data df contains unique taxon_ids
        # columns = [taxon_id, parent_taxon_id, taxon_rank, latin_name, english_name]
        # left join is used in order to leave taxa without english names provided in the data
        self.data = pd.merge(taxa, names, on="taxon_id", how="left").fillna("")
        self.class_rows = self.data.loc[self.data["taxon_rank"] == "class"].fillna("")
        self.classes = self.class_rows["latin_name"].tolist()

    def concatenate_taxon_name(self, row):
        generic = row["dwc:genericName"]
        # for ranks higher that species
        if not generic:
            return row["dwc:scientificName"].split()[0]

        res = generic
        infrageneric = row["dwc:infragenericEpithet"]
        if infrageneric:
            res += f" ({infrageneric})"
        specific = row["dwc:specificEpithet"]
        if specific:
            res += f" {specific}"
        infraspecific = row["dwc:infraspecificEpithet"]
        if infraspecific:
            res += f" {infraspecific}"
        return res

    def get_next_class(self, class_name):
        ind = self.classes.index(class_name)
        return self.classes[ind+1] if ind < len(self.classes) - 1 else None

    def export_class(self, class_name):
        """
        A slice is taken from self.data df, containing taxa belonging to a single class: starting from the row
        specifying class and ending with a last row before the next class listing starts.
        :param class_name:
        :return:
        """
        if class_name not in self.CHORDATA_CLASSES:
            raise Exception("Class not found in taxonomy data")

        class_row = self.class_rows.loc[self.class_rows["latin_name"] == class_name]
        class_row_index = class_row.index.values[0]
        next_class = self.get_next_class(class_name)
        if next_class is None:
            next_class_row_index = self.data.shape[0]
        else:
            next_class_row_index = self.class_rows.loc[self.class_rows["latin_name"] == next_class].index.values[0] - 1

        df = self.data[class_row_index:next_class_row_index]
        return df[["taxon_id", "taxon_rank", "latin_name", "english_name"]]

    def generate_files(self):
        for cls in self.CHORDATA_CLASSES:
            df = self.export_class(cls)
            df.to_csv(f"data/{cls}.csv")
            print(f"generated file for {cls}")
